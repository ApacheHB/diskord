package com.serebit.diskord.network

internal enum class PostCloseAction {
    RESUME, RESTART, CLOSE
}
