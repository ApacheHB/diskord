package com.serebit.diskord

internal typealias Snowflake = Long
internal typealias IsoTimestamp = String
internal typealias BitSet = Int
internal typealias UnixTimestamp = Long
