package com.serebit.diskord.entities

interface Entity {
    val id: Long
}
