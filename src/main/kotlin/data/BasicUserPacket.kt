package com.serebit.diskord.data

import com.serebit.diskord.Snowflake

internal data class BasicUserPacket(val id: Snowflake)
