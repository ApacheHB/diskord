package com.serebit.diskord.data

import com.serebit.diskord.Snowflake

interface EntityPacket {
    val id: Snowflake
}